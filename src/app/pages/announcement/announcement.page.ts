import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { NoticeUp, MOSQUE_REGISTER_INTERFACE, FavoriteAnnoucementInterface,Announcement } from 'src/app/Helper classes/Interfaces/interface';
import { Router } from '@angular/router';
import { ServiceManager } from 'src/app/WebServices/service-manager.service';
import { SharedService } from 'src/app/shared.service';
import { NavController } from '@ionic/angular';
// import { Content } from '@angular/compiler/src/render3/r3_ast';


@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.page.html',
  styleUrls: ['./announcement.page.scss'],
})
export class AnnouncementPage implements OnInit {

  
  favoriteAnnoucments: Announcement[] = []
  
  constructor(
    private router: Router,
    private serviceManager: ServiceManager,
    public shared: SharedService,
    private zone: NgZone,
    private navControler: NavController,
  ) { }

  ngOnInit() {
    
    // this.setUpDummyData();
  }

  ionViewWillEnter() { 
    this.getFavoriteAnnoucment()
  }
  getFavoriteAnnoucment(){

    
    let params = {
      service: btoa('lst_user_announcements'),
      u_id: btoa(this.shared.MussaliLogin.u_id.toString()),
      last_fetched: btoa("")
    }
    this.serviceManager.showProgress()

    this.serviceManager.getData(params).then(res => { 
      this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) { 
        let response: FavoriteAnnoucementInterface = JSON.parse(res.data)
        if (response.announcements.length > 0) {
          this.favoriteAnnoucments = response.announcements
        }
      }
    }, err => { 
        console.log(err);
        
        this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
      /** 
      
      .subscribe((res: FavoriteAnnoucementInterface) => {
      console.log(JSON.stringify(res.json_status.value));
      this.serviceManager.hideProgress()
      console.log(res);
      if (res.announcements.length > 0) {
        this.favoriteAnnoucments = res.announcements
      } else {
        // show no post found message
      }

    })
    */
    
  }
  goBack() { 

    this.navControler.navigateRoot('/auth-login')
  }
  getformatedDate(dateStr: string): string {
    console.log(dateStr);

    return this.serviceManager.AvailableSlotsFor(dateStr)
  }
  private setUpDummyData() {
    let noti1: NoticeUp = {
      id: 1,
      title: 'Event on coming friday',
      message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
      noticeDate: 'Nov 23,2019 '
    };
    let noti2: NoticeUp = {
      id: 2,
      title: 'Funds Collection',
      message: 'Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text.Contrary to popular belief, Lorem Ipsum is not simply random text.',
      noticeDate: 'Nov 29,2019 '
    };
    let noti3: NoticeUp = {
      id: 3,
      title: 'Nouman Ali Khan is coming',
      message: 'There are many variations of passages of Lorem Ipsum available.',
      noticeDate: 'Nov 30,2019 '
    };
    let noti4: NoticeUp = {
      id: 4,
      title: 'Funds Collections',
      message: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced.',
      noticeDate: 'Dec 3,2019 '
    };
    // this.favoriteAnnoucments.push(noti1);
    // this.favoriteAnnoucments.push(noti2);
    // this.favoriteAnnoucments.push(noti3);
    // this.favoriteAnnoucments.push(noti4);
  }

  getAnnouncementForUser() {
    let params = {
      service: btoa('lst_announcements_for_user'),
      u_id: btoa('4')
    }
    this.serviceManager.showProgress("Fetching Data...")
    this.serviceManager.getData(params).then(res => { 
      if (res.status = 200 && res.data) { 
        let response: MOSQUE_REGISTER_INTERFACE = JSON.parse(res.data)
        if (response.status == '1') {
          console.log('Register Done')
          this.router.navigateByUrl('admin');
        } else {
          this.serviceManager.makeToastOnFailure(response.message)
        }
      }
    }, err => { 
        console.log(err);
        
        this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
      /*
      .subscribe((res: MOSQUE_REGISTER_INTERFACE) => {
      console.log(JSON.stringify(res.status == '1'));
      if (res.status == '1') {
        console.log('Register Done')
        this.router.navigateByUrl('admin');
      } else {
        this.serviceManager.makeToastOnFailure(res.message)
        // alert('looks like got an error')
      }

    })  
    */
  }

 
}
