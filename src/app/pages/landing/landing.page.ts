import { Component, OnInit } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { Router,NavigationExtras } from '@angular/router';
import { debug } from 'util';
import { ServiceManager } from 'src/app/WebServices/service-manager.service';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import { MussaliLoginInterface } from 'src/app/Helper classes/Interfaces/interface';
import { SharedService } from 'src/app/shared.service';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {
  userLatitude = ''
  userLongitude = ''

   options: NativeTransitionOptions = {
    direction: 'up',
    duration: 500,
    slowdownfactor: 3,
    slidePixels: 20,
    iosdelay: 100,
    androiddelay: 150,
    fixedPixelsTop: 0,
    fixedPixelsBottom: 60
   }
  constructor(
    private serviceManager: ServiceManager,
    // private geolocation: Geolocation,

    private router: Router,
    public shared: SharedService,
    public platform: Platform,
    private navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions
  ) { 
    
    // this.shared.loggedInAdmin = null
    // this.shared.MussaliLogin = null
    // this.shared.selectedPlaceDetail  =  null
    // this.serviceManager.removeFromStorageForTheKey(this.serviceManager.LOGGED_IN_AS_Admin)
  }

  ngOnInit() {
    


  }
  ionViewWillEnter() {
    // this.platform.ready().then(() => {
    //   this.fetchLocation()
    //   this.fetchLocation()
    //   this.fetchLocation()
    //   this.fetchLocation()
    //   this.fetchLocation()
    //   this.fetchLocation()
    //   this.fetchLocation()
    // })
  }

 
   
//   fetchLocation() {
//     let gpsOptions = { maximumAge: 300000, timeout: 30000, enableHighAccuracy: true };
//     this.geolocation.getCurrentPosition(gpsOptions).then((resp) => {
//       // alert('got location')
//       console.log(resp.coords.latitude);
//       console.log(resp.coords.longitude);
//       this.userLatitude = resp.coords.latitude.toString()
//       this.userLongitude = resp.coords.longitude.toString()

//     }).catch((error) => {
//       console.log(error);
//      });
     
//      let watch = this.geolocation.watchPosition();
//      watch.subscribe((data) => {
//       // data can be a set of coordinates, or an error (if an error occurred).
//       this.userLatitude = data.coords.latitude.toString()
//       this.userLongitude = data.coords.longitude.toString()
//       // alert("got position in  watch position")
//      });
// }
  navigateToLoginScreen() { 
    this.nativePageTransitions.slide(this.options);
  }
  
  moveToMussalModuleTapped() {
// porridge
    // zaheer sb
     // Mosque app
     // learn backend
    // this.router.navigateByUrl('mussali-landing-page');
    // if (this.userLatitude.trim().length == 0) { 
    //   this.serviceManager.makeToastOnFailure('was unable to fetch your locaiton. please check your configuration.')
    //   // this.userLatitude = "31.499413"
    //   // this.userLongitude = "74.3492497"
    //   // this.fetchLocation()
    //   return 
    // }
    // this.shared.GCM_TOKEN = '2332kl3e423fdjsfior23ufwkdhyr3pqehiwfkdvs3y89reowfhdsvp8yrewfhdv894yhn'
    if (this.shared.GCM_TOKEN.trim().length == 0) { 
      this.shared.GCM_TOKEN = '2332kl3e423fdjsfior23ufwkdhyr3pqehiwfkdvs3y89reowfhdsvp8yrewfhdv894yhn'
    }
    let params = {
      service: btoa('register_user'),
      u_fcm_token: btoa(this.shared.GCM_TOKEN),
      u_phone: btoa(''),
      // u_lat: btoa(this.userLatitude),
      // u_lng: btoa(this.userLongitude),
      
      
    }
    this.serviceManager.showProgress()
    this.serviceManager.getData(params).then(res => { 
      this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) { 
        let response: MussaliLoginInterface = JSON.parse(res.data)
        if (response.status == '1') {
          let customerData = JSON.parse(res.data)
          this.serviceManager.setInLocalStorage(this.serviceManager.LOGGED_IN_AS_MUSSALI, true)
          this.serviceManager.setInLocalStorage(this.serviceManager.CUSTOMER_DATA, customerData)
          this.shared.MussaliLogin = response
          this.serviceManager.makeToastOnSuccess(response.message)
          this.navCtrl.navigateRoot('mussali-landing-page')
        } else {
          this.serviceManager.hideProgress()
          this.serviceManager.makeToastOnFailure(res.error)
        }
        
      }
    }, err => { 
        console.log(err);
        
        this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
    
    
  }
}
