import { Component, OnInit } from '@angular/core';
import { NoticeUp, loginResponse, AnnouncementListinResponse, AnnouncementListing } from 'src/app/Helper classes/Interfaces/interface';
import { SharedService } from 'src/app/shared.service';
import { ServiceManager } from 'src/app/WebServices/service-manager.service';
import { debug } from 'util';
import { PlacesListing } from '../../Helper classes/Interfaces/interface';
import { ModalController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
})
export class AdminPage implements OnInit {

  notficationDatasource: AnnouncementListing[] = []
  mosqueData: loginResponse = null
  placeId = ""
  place: PlacesListing
  imageBaseUrl = null
  segmentPlaceDetail = 'isAnnoucement'
  constructor(
    public shared: SharedService,
    private serviceManager: ServiceManager,
    public modalCtrl: ModalController,
    private router: Router,
    private navControler: NavController,
  ) { }

  ngOnInit() {
    this.populateData()
  }

  populateData() {

    if (this.shared.selectedPlaceDetail) {
      this.place = this.shared.selectedPlaceDetail
      this.imageBaseUrl = "./assets/imgs/" + this.place.p_email + ".jpg"
    } else {
      this.mosqueData = this.shared.loggedInAdmin

      if (this.mosqueData) {
        this.place = {
          p_id: this.mosqueData.p_id,
          p_description: this.mosqueData.p_description,
          p_name: this.mosqueData.p_name

        }
        this.imageBaseUrl = "./assets/imgs/" + this.mosqueData.p_email + ".jpg"
      }
    }

  }
  ionViewWillEnter() {

    let isPlaceUpdated = (this.serviceManager.getFromLocalStorage('this.shared.placeDataUpdated'))
    if (isPlaceUpdated) {
      this.populateData()
      this.serviceManager.setInLocalStorage('this.shared.placeDataUpdated', false)
    }

    this.getMosqueDetail()
  }
  dide
  ionViewWillLeave() {
    if (!this.mosqueData) {
      this.shared.selectedPlaceDetail = null

    }
  }
  getMosqueDetail() {


    let params = {
      service: btoa('lst_announcements'),
      p_id: btoa(this.place.p_id),

    }
    this.serviceManager.showProgress()

    this.serviceManager.getData(params).then(res => {
      this.serviceManager.hideProgress()
      this.serviceManager.hideProgress()
      this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) {
        let response: AnnouncementListinResponse = JSON.parse(res.data)
        if (response.announcements.length > 0) {
          this.notficationDatasource = response.announcements
        }
      }
    }, err => {
      console.log(err);
      this.serviceManager.hideProgress()
      this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
    /*
    .subscribe((res: AnnouncementListinResponse) => {
    console.log(JSON.stringify(res.json_status.value));
    this.serviceManager.hideProgress()
    console.log(res);
    if (res.announcements.length > 0) {
      this.notficationDatasource = res.announcements
    } else {
      // show no post found message
    }

    })
  */


  }
  getformatedDate(dateStr: string): string {
    console.log(dateStr);

    return this.serviceManager.AvailableSlotsFor(dateStr)
  }
  BackToMosquTab() {
    this.navControler.pop()
    // this.modalCtrl.dismiss();
  }
  // 
  getTrimmedValue(desription: string) {
    return desription.slice(0, 120)
  }

  isShowingAnnoucement = true
  segmentChanged(event) {
    if (event.detail.value == 'isAnnoucement') {
      this.isShowingAnnoucement = true
    } else {
      this.isShowingAnnoucement = false
    }

  }
  navigateToNewAnnouncement() { 

    this.navControler.navigateForward("/new-annoucement")
  }
}
