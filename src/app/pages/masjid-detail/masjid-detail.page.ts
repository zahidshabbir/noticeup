import { Component, OnInit } from '@angular/core';
import { MasjidObject, NoticeUp, PlacesListing, AnnouncementListinResponse, AnnouncementListing, MOSQUE_REGISTER_INTERFACE } from 'src/app/Helper classes/Interfaces/interface';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { SharedService } from 'src/app/shared.service';
import { ServiceManager } from 'src/app/WebServices/service-manager.service';

@Component({
  selector: 'app-masjid-detail',
  templateUrl: './masjid-detail.page.html',
  styleUrls: ['./masjid-detail.page.scss'],
})


export class MasjidDetailPage implements OnInit {
  selectedPlace: PlacesListing = null

  notficationDatasource: AnnouncementListing[] = []
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public datePipe: DatePipe,
    public shared: SharedService,
    private serviceManager: ServiceManager,
  ) {
    this.selectedPlace = this.shared.selectedPlaceDetail
    
    // this.activatedRoute.queryParams.subscribe(params => {
    //   if (this.router.getCurrentNavigation().extras.state) {
    //     this.place = this.router.getCurrentNavigation().extras.state.selectedPlace;
    //     console.log(this.place);
      
    //   }
    // });
  }
  ngOnInit() {
    
    // this.setupDummyData();
    this.getPlaceEents()
  }

  // private setupDummyData() {
  //   for (let index = 0; index < 10; index++) {
  //     let testDate = new Date().toLocaleString();
  //     console.log(this.getFormattedDate(testDate));
  //   }
  //   let zahid = this.activatedRoute.snapshot.paramMap.get('masjid');
  //   // console.log(zahid);
  //   let noti1: NoticeUp = {
  //     id: 1,
  //     title: 'Test Notice 1',
  //     message: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
  //     noticeDate: '23 Nov, 2019 : 12:35:00'
  //   };
  //   let noti2: NoticeUp = {
  //     id: 2,
  //     title: 'Test Notice 2',
  //     message: 'Contrary to popular belief, Lorem Ipsum is not simply random text.',
  //     noticeDate: 'Thu Nov 01 2019 02:00:00'
  //   };
  //   let noti3: NoticeUp = {
  //     id: 3,
  //     title: 'Test Notice 3',
  //     message: 'There are many variations of passages of Lorem Ipsum available.',
  //     noticeDate: 'Fri Jul 12 2019 21:23:18'
  //   };
  //   let noti4: NoticeUp = {
  //     id: 4,
  //     title: 'Test Notice 4',
  //     message: 'The standard chunk of Lorem Ipsum used since the 1500s is reproduced.',
  //     noticeDate: '18 Nov, 2019 : 12:35:00'
  //   };
  //   this.notficationDatasource.push(noti1);
  //   this.notficationDatasource.push(noti2);
  //   this.notficationDatasource.push(noti3);
  //   this.notficationDatasource.push(noti4);
  // }

  
  makritAsFavroite() {
     (!this.selectedPlace.favplaces || this.selectedPlace.favplaces == "0") ? this.selectedPlace.favplaces = "1" : "0"
    
    let params = {
      service: btoa('sub_place'),
      u_id: btoa(this.shared.MussaliLogin.u_id.toString()),
      p_id: btoa(this.selectedPlace.p_id),
      sub_status: btoa(this.selectedPlace.favplaces.toString())
    }
    this.serviceManager.showProgress()
    this.serviceManager.getData(params).then(res => { 
      this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) { 
        let response: MOSQUE_REGISTER_INTERFACE = JSON.parse(res.data)
        if (response.status == '1') {
          console.log('Register Done')
          this.serviceManager.makeToastOnSuccess(response.message)
          // return this.places[index].favplaces
        } else {
          this.serviceManager.makeToastOnFailure(response.message)
        }
      }
    }, err => { 
        console.log(err);
        
        this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
     /* .subscribe((res: MOSQUE_REGISTER_INTERFACE) => {
      console.log(JSON.stringify(res.status == '1'));
      if (res.status == '1') {
        console.log('Register Done')
        this.serviceManager.makeToastOnSuccess(res.message)
        // return this.places[index].favplaces
      } else {
        this.serviceManager.makeToastOnFailure(res.message)
      }

    })  */
    
  }
  getPlaceEents(){

    
    let params = {
      service: btoa('lst_announcements'),
      p_id: btoa(this.selectedPlace.p_id),
 
    }
    this.serviceManager.showProgress()

    this.serviceManager.getData(params).then(res => { 
      this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) { 
        let response: AnnouncementListinResponse = JSON.parse(res.data)
        if (response.announcements.length > 0) {
          this.notficationDatasource = response.announcements
          this.notficationDatasource[0].a_description
        } else {
          // show no post found message
        }
      }
    }, err => { 
        console.log(err);
        
        this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
      /*.subscribe((res: AnnouncementListinResponse) => {
      console.log(JSON.stringify(res.json_status.value));
      this.serviceManager.hideProgress()
      console.log(res);
      if (res.announcements.length > 0) {
        this.notficationDatasource = res.announcements
        this.notficationDatasource[0].a_description
      } else {
        // show no post found message
      }

    })
    */
    
  }
  
   getFormattedDate(appointmentTime) {
    appointmentTime = appointmentTime.replace(/-/g, '/');
    if (appointmentTime === '0000/00/00 00:00:00') {
      return 'N/A';
    }

    appointmentTime = appointmentTime.replace(/-/g, '/');
    
      // return this.datePipe.transform(appointmentTime, 'EEE, dd MMM (hh:mm a)');
    
      return this.datePipe.transform(appointmentTime, 'EEE MMM dd yyyy HH:mm:ss');
      // Fri, 12 Jul (09:19 PM)
      // Fri Jul 12 (21:20)
  }
}

