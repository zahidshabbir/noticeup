import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MasjidDetailPageRoutingModule } from './masjid-detail-routing.module';

import { MasjidDetailPage } from './masjid-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MasjidDetailPageRoutingModule
  ],
  declarations: [MasjidDetailPage]
})
export class MasjidDetailPageModule {}
