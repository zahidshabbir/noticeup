import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MasjidDetailPage } from './masjid-detail.page';

describe('MasjidDetailPage', () => {
  let component: MasjidDetailPage;
  let fixture: ComponentFixture<MasjidDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasjidDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MasjidDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
