import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MasjidDetailPage } from './masjid-detail.page';

const routes: Routes = [
  {
    path: '',
    component: MasjidDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MasjidDetailPageRoutingModule {}
