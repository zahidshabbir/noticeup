import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewAnnoucementPageRoutingModule } from './new-annoucement-routing.module';

import { NewAnnoucementPage } from './new-annoucement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewAnnoucementPageRoutingModule
  ],
  declarations: [NewAnnoucementPage]
})
export class NewAnnoucementPageModule {}
