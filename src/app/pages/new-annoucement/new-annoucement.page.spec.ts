import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewAnnoucementPage } from './new-annoucement.page';

describe('NewAnnoucementPage', () => {
  let component: NewAnnoucementPage;
  let fixture: ComponentFixture<NewAnnoucementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAnnoucementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewAnnoucementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
