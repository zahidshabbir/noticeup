import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceManager } from 'src/app/WebServices/service-manager.service';
import { SharedService } from 'src/app/shared.service';
import { loginResponse, NewAnnoucenment } from 'src/app/Helper classes/Interfaces/interface';
import { NavController } from '@ionic/angular';
import { UPDATE_PLACE_MODEL_RESPONSE, Place } from '../../Helper classes/Interfaces/interface';
// import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-new-annoucement',
  templateUrl: './new-annoucement.page.html',
  styleUrls: ['./new-annoucement.page.scss'],
})
export class NewAnnoucementPage implements OnInit {

  noticeTitleTextField = ''
  noticeDescriptionTextField = ''
  expirayDate = ''
  place: loginResponse = null
  imageBaseUrl = null
  constructor(
    private router: Router,
    private serviceManager: ServiceManager,
    public shared: SharedService,
    private navControler: NavController,
    // private keyboard: Keyboard,
  ) { }

  ngOnInit() {
    this.place = this.shared.loggedInAdmin
    this.imageBaseUrl = "./assets/imgs/" + this.place.p_email + ".jpg"
  }

  postAnnoucementButtonTappd() {
    if (this.noticeTitleTextField.trim().length == 0) {
      this.serviceManager.makeToastOnFailure('Title is required.')
      return
    }

    if (this.noticeDescriptionTextField.trim().length == 0) {
      this.serviceManager.makeToastOnFailure('Description is also required.')
      return
    }
    this.expirayDate = this.serviceManager.getCurrentDeviceDateTime()
    let params = {
      service: btoa('add_announcement'),
      a_title: btoa(this.noticeTitleTextField),
      a_description: btoa(this.noticeDescriptionTextField),
      p_id: btoa(this.place.p_id),
      a_expiry: btoa(this.expirayDate),



    }
    this.serviceManager.showProgress("Sharing your post...")
    this.serviceManager.getData(params).then(res => {
      if (res.status = 200 && res.data) {
        this.serviceManager.hideProgress()
        let response: UPDATE_PLACE_MODEL_RESPONSE = JSON.parse(res.data)
        debugger
        if (response.status == '1') {
          this.navControler.pop()
          this.serviceManager.makeToastOnSuccess('Your post is shared with nearby mussali')
        } else {
          this.serviceManager.makeToastOnFailure(response.message)
        }

      }
    }, err => {
      console.log(err);

      this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
    
  }
 
  backButtonTapped() {

    this.navControler.pop()
    this.router.dispose()
  }
  dateSelected(event) {
    this.expirayDate = this.serviceManager.getAnnoucementExpiryDate(event.detail.value)
  }
}
