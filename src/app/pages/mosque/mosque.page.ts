import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { MOSQUE_REGISTER_INTERFACE, PlacesListing, MosqueResponseInterface } from 'src/app/Helper classes/Interfaces/interface';
import { ServiceManager } from 'src/app/WebServices/service-manager.service';
import { SharedService } from 'src/app/shared.service';
import { AdminPage } from '../admin/admin.page';

@Component({
  selector: 'app-mosque',
  templateUrl: './mosque.page.html',
  styleUrls: ['./mosque.page.scss'],
})
export class MosquePage implements OnInit {
  places: PlacesListing[] = []
  constructor(
    private navControler: NavController,
    private router: Router,
    private serviceManager: ServiceManager,
    public shared: SharedService,
    public modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    // alert(this.shared.MussaliLogin.u_id.toString())
  }



  ionViewWillEnter() {

    this.getMosqueAroundUser()

    // this.setupDummyData();
  }

  // private setupDummyData() {
  //   this.myMasjidMasajidDataSource = [];
  //   this.myMasjid = {};
  //   this.myMasjid.id = 1;
  //   this.myMasjid.name = "London Central Mosque";
  //   this.myMasjid.image = "./assets/imgs/m1.jpg";
  //   this.myMasjid.liked = true;
  //   this.myMasjid.description = "The London Central Mosque is a mosque located near Regent's Park in London, United Kingdom. It was designed by Sir Frederick Gibberd, completed in 1977, and has a prominent golden dome. The main hall can accommodate over 5,000 worshippers";
  //   let m2: MasjidObject = {
  //     id: 2,
  //     name: "Fazl Mosque /The London Mosque",
  //     image: "./assets/imgs/m2.jpg",
  //     description: "The Fazl Mosque, also known as The London Mosque, is the first purpose-built mosque in the British capital. It was inaugurated on 23 October 1926 in Southfields, Wandsworth.",
  //     liked: false
  //   };
  //   let m3: MasjidObject = {
  //     id: 3,
  //     name: "London Central Mosque",
  //     image: "./assets/imgs/m3.jpg",
  //     description: "Also known as the Islamic Cultural Centre, ICC or Regent's Park Mosque",
  //     liked: true
  //   };
  //   let m4: MasjidObject = {
  //     id: 4,
  //     name: "East London Mosque",
  //     image: "./assets/imgs/m4.jpg",
  //     description: "One of the few mosques in Britain permitted to use loudspeakers to broadcast the call to prayer.",
  //     liked: true
  //   };
  //   this.myMasjidMasajidDataSource.push(this.myMasjid);
  //   this.myMasjidMasajidDataSource.push(m2);
  //   this.myMasjidMasajidDataSource.push(m3);
  //   this.myMasjidMasajidDataSource.push(m4);
  // }

  makritAsFavroite(place: PlacesListing, index: number) {
    console.log(place, index);
    // u_id, p_id, sub_status

    // (place.favplaces === null || place.favplaces == "0") ? place.favplaces = "1" : "0"

    if (place.favplaces == null || place.favplaces === "0") {
      place.favplaces = "1"
    } else {
      place.favplaces = "0"
    }
    this.places[index] = place

    let params = {
      service: btoa('sub_place'),
      u_id: btoa(this.shared.MussaliLogin.u_id.toString()),
      p_id: btoa(place.p_id),
      sub_status: btoa(this.places[index].favplaces.toString())
    }
    // this.serviceManager.showProgress("Adding to favorite...")
    this.serviceManager.getData(params).then(res => {
      // this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) {
        let response: MOSQUE_REGISTER_INTERFACE = JSON.parse(res.data)
        if (response.status == '1') {
          console.log('Register Done')
          this.getMosqueAroundUser()
          // return this.places[index].favplaces
        } else {
          this.serviceManager.makeToastOnFailure(response.message)
          // alert('looks like got an error')
        }
      }
    }, err => {
      console.log(err);

      this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
    /*.subscribe((res: MOSQUE_REGISTER_INTERFACE) => {
    console.log(JSON.stringify(res.status == '1'));
    if (res.status == '1') {
      console.log('Register Done')
      // return this.places[index].favplaces
    } else {
      this.serviceManager.makeToastOnFailure(res.message)
      // alert('looks like got an error')
    }

  })  
  */
    //   this.places.map((currentPlace) => {

    //     if (currentPlace == place) {
    //       currentPlace.favplaces = !place.expanded;
    //     } else {
    //       cateogry.expanded = false;
    //     }

    //     return cateogry;

    //   });
    // }

    // return this.places[index] = place
  }

  MasjidDetailTapped(place: PlacesListing, i: number) {
    this.shared.selectedPlaceDetail = place

    this.router.navigateByUrl('admin');
  }

  getMosqueAroundUser() {
    // lst_announcements
    let params = {
      service: btoa('lst_places'),
      u_id: btoa(this.shared.MussaliLogin.u_id.toString())
    }
    this.serviceManager.showProgress("Fetching near by mosques")
    this.serviceManager.getData(params).then(res => {
      this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) {
        let response: MosqueResponseInterface = JSON.parse(res.data)
        if (response.status == '1') {
          console.log(res);
          this.places = response.places

        } else {

          this.serviceManager.makeToastOnFailure("something went wrong")
          // alert('looks like got an error')
        }
      }
    }, err => {
      console.log(err);

      this.serviceManager.makeToastOnSuccess("Something went wrong")
    })
    /*
    .subscribe((res: MosqueResponseInterface) => {
    this.serviceManager.hideProgress()
    console.log(JSON.stringify(res.status == '1'));
    if (res.status == '1') {
      console.log(res);
      this.places = res.places
      // this.places.forEach((place, index) => {
      //   place.favplaces = Number(place.favplaces)
      //   return place
      // });
      
    } else {

      this.serviceManager.makeToastOnFailure("something went wrong")
      // alert('looks like got an error')
    }

  })  
  */
  }
  getMosueImageURl(place: PlacesListing) {
    return "./assets/imgs/" + place.p_email + ".jpg"
  }
}