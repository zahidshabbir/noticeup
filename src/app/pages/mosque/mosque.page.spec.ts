import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MosquePage } from './mosque.page';

describe('MosquePage', () => {
  let component: MosquePage;
  let fixture: ComponentFixture<MosquePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MosquePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MosquePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
