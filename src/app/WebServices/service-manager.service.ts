
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';


import { config } from './config';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';

import { ToastController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { debug } from 'util';

const API: string = config.BaseURL
const NOTI: string = "https://nopso.qwpcorp.com/salon/push_test_1.php";

@Injectable({
  providedIn: 'root'
})
export class ServiceManager {
  isHideTab: string;
  TabsIndex: string = "";
  api_key: string = "";


  dismissOnPageChange = false
  headers: Headers
  // options: RequestOptions
  // options: HttpHeaders∂
  toast: HTMLIonToastElement;

  /*
 *  user app  Global Constants
 */
  LOGGED_IN_AS_MUSSALI = "user_is_logged_in_as_mussalli"
  LOGGED_IN_AS_Admin = "user_is_logged_in_as_admin"
  
  CUSTOMER_DATA = 'customer'
  ADMIN_DATA = 'logged_admin_data'


  
  
  /*
   *  user app  Global Constants
   */


  loader: HTMLIonLoadingElement
  alreadyLoading: boolean = false
  toastAlreadyShowing = false
  DEVICE_DATETIME = 'device_datetime'
  LOGED_IN_SALON_DATA = 'salon_data'
  SALON_TECHS = 'salon_techs'
  LOGED_IN_SALON_OFFERS = 'salon_offers'
  LOGED_IN_SALON_PROFILE_IMG = 'salon_profile_img'

  LOGED_IN_SALON_TECHS_SERVICES = 'techs_services'
  LOGED_IN_SALON_SALON_SETTINGS = 'salon_Settings'
  GCM_TOKEN = 'gcmToken'
  INVALID_POSTCODE = 'invalid postcode'
  SLOW_INTERNET_ERROR = 'Cannot reach server due to no/slow internet. Please try after a while.'
  NO_POST_FOR_SUBSTYLE_FOUND = 'no post for this sub-style'
  OBJ_SUB_STYLE = 'subStyle_object'
  OBJ_PIN = 'pin_object'
  Customer_Gender = ''
  CUSTOMER_GENDER_MALE = '1'
  CUSTOMER_GENDER_FEMALE = '2'
  NEAREST_SALON_ASSOCIATED_WITH_PIN = 'sal_associated_with_pin'
  SALONS_NEAR_CUSTOMER = 'salo_near_customer'
  // SALON_DATA='salons'
  LAST_FETCH_DATE_SALONS = 'salon_last_fetched_on'
  NEAREST_SALON_SERVICES = 'sal_services'
  APP_STATUS_SERVING = 'serving'
  APP_STATUS_FINISHED = 'finished'
  APP_STATUS_ACCEPTED = 'accepted'
  APP_STATUS_REJECTED = 'rejected'
  APP_STATUS_DELETED = 'deleted'
  APP_STATUS_PENDING = 'pending'
  ACCEPT_BUTTON_TEXT = 'Accept'
  REMOVE_BUTTON_TEXT = 'Cancel'
  REJECT_BUTTON_TEXT = 'Reject'
  RESCHEDULE_BUTTON_TEXT = 'Reschedule'



  SUCCESS_STATUS = '1'
  FAILURE_STATUS = '0'
  IS_LOGGED_IN = 'looged_in'
  LOGGED_IN_SALON_ID = 'logged_in_salon_id'
  LOGGED_IN_SALON_NAME = 'logged_in_salon_name'
  LOGGED_IN_SALON_STATUS = 'logged_in_salon_status'
  LOGGED_IN_SALON_ADDRESS = 'logged_in_salon_address'

  SAL_24CLOCK = 'sal_24clock'
  SAL_AUTO_ACCEPT_APP = 'sal_auto_accept_app'
  SAL_FUTURE_APP_DAYS = 'sal_future_app_days'
  NO_MATCH_FOUND = 'No match found.'

  NO_APPOINTMENTS = 'No appointment exists.'


  IS_MY_ORDER_REQUESTED_ALREADY_INITIATED = 'is_my_order_First_Call'
  IS_PRODUCTS_FIRST_LAUNCH = 'is_product_first_Call'

  IS_FORCE_UPDATE_AVAILABLE = 'IS_FORCE_UPDATE_AVAILABLE'
  FORCE_UPDATE_MESSAGE = 'FORCE_UPDATE_MESSAGE'


  constructor(
    private toastController: ToastController,
    public loadingCtrl: LoadingController,
    public datePipe: DatePipe,
    // public http: Http,

    private httpClient: HttpClient,
    private http: HTTP,
    private storage: Storage

  ) {
    // this.storage.get('api_key').then((value) => {
    //   if (value != undefined && value != null) {
    //     this.api_key = value;
    //     // this.base64Image = value;//MainHomePage;
    //   }
    // })


    this.isHideTab = "0";
    this.setUpHeaderAndOptions()
  }

  // CUSTOME FUNCTIONS

  getDateFormattedForAppointmentConfirmation(dateString) {
    return this.datePipe.transform(dateString, 'dd MMMM yyy');
  }
  AvailableSlotsFor(date: any): string {
    let selectedDate = date.replace(/-/g, '/');
    const format = 'dd/MM/yyyy HH:mm'
    return this.datePipe.transform(selectedDate, format);
  }
  getFormattedTimeForMyorder(date: any): string {
    let selectedDate = date.replace(/-/g, '/');
    return this.datePipe.transform(selectedDate, 'dd/MM/yyyy');
  }
 

  getCurrentDeviceDateTime(): string {
    const format = 'yyyy-MM-dd HH:mm:ss'
    const currentDate = new Date();

    return this.datePipe.transform(currentDate, format)

  }

  getCurrentDeviceDate(date: string): string {

    const format = 'yyyy-MM-dd'

    let currentDate: Date

    if (date.toString().trim().length === 0) {
      currentDate = new Date()
    } else {
      currentDate = new Date(date);
      if (currentDate === undefined) {
        return ''
      }
    }
    if (currentDate === undefined) {
      return ''
    }
    return this.datePipe.transform(currentDate, format)

  }
  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      animated: true,
      closeButtonText: 'Cancel',
      color: 'primary',
      cssClass: 'toast-success',
      duration: 2000,
      header: 'Toast header',
      keyboardClose: true,
      message: 'Click to Close',
      mode: 'ios',
      position: 'bottom',
      showCloseButton: true,
      translucent: true
    });
    toast.present();


  }
  async makeToastOnSuccess(message: string, headerMsg: string = "MasjidApp") {


    if (this.toastAlreadyShowing) { return false }
    this.toast = await this.toastController.create({
      animated: true,
      // closeButtonText: 'Cancel',
      // color: 'primary',
      cssClass: 'toast-on-success',
      duration: 3000,
      header: headerMsg,
      keyboardClose: true,
      message: message,
      mode: 'ios',
      position: 'top',
      showCloseButton: false,
      translucent: true,


    });

    this.toast.present();
    this.toastAlreadyShowing = true
  }

  async makeToastOnFailure(message: string, headerMsg: string = "MasjidApp") {

    this.toast = await this.toastController.create({
      animated: true,
      // closeButtonText: 'Cancel',
      // color: 'primary',
      cssClass: 'toast-on-failure',
      duration: 4000,
      header: headerMsg,
      keyboardClose: true,
      message: message,
      mode: 'ios',
      position: 'top',
      showCloseButton: false,
      translucent: true
    });

    this.toast.present();

    this.toastAlreadyShowing = true


  }

  async showProgress(message: string = "Please wait...") {
  
    if (this.alreadyLoading) { return false }
    console.log('alread Loading: ', this.alreadyLoading);


    this.loader = await this.loadingCtrl.create({
      spinner: null,
      duration: 15000,
      message: message,
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });

    this.alreadyLoading = true
    console.log("loader presented");
    return await this.loader.present();
  }


  hideProgress() {
    if (!this.alreadyLoading) { return false }
    try {
      console.log('alread Loading: ', this.alreadyLoading);
      console.log('loader dismissed');
      this.loader.dismiss()

      this.alreadyLoading = false
    }
    catch (Exception) {
      console.log('cannot dismiss loader ');
    }
  }

  setInLocalStorage(key: string, Value: any) {
   
    // if (typeof (Value) === 'string') {
    //   localStorage.setItem(key, Value)
    // } else { 
      localStorage.setItem(key, JSON.stringify(Value))
    // }
    
  }
  getFromLocalStorage(key: string): any {

    const obj = localStorage.getItem(key)
    if (obj === undefined) {
      return obj
    }
    return JSON.parse(obj)
  }
  
  // setInLocalStorage(key: any, Value: any) {

  //   this.storage.set(key, JSON.stringify(Value)).then(success => { 
  //     console.log('job deonnnn.');
  //     // alert(key +  ' => ' + Value)
  //   }, er => { 
  //       console.log('could not save');
        
  //   });
  //   // localStorage.setItem(key, JSON.stringify(Value))
  // }
  // getFromLocalStorage(key: string): any {
    
  //   // this.storage.get(key).then(val => { 
  //   //   alert(key +  ' == ' + val)
  //   // })

  //   return this.storage.get(key)
  //   // const obj = localStorage.getItem(key)
  //   // if (obj === undefined) {
  //   //   return obj
  //   // }
  //   // return JSON.parse(obj)
  // }

  removeItemFromStorage(postsKey: string, paginateKey: string, startTimeKey: string) {
    localStorage.removeItem(postsKey);
    localStorage.removeItem(paginateKey);
    localStorage.removeItem(startTimeKey);

  }
  removeFromStorageForTheKey(obj: string) {
    localStorage.removeItem(obj);
  }
  // getDataFromLaravelService(url) {
  //   return this.http.get(url, this.options)
  //     .map(response => response.json());
  // }

  getData(params): Promise<HTTPResponse> {
    console.log('without encoded');
    console.log(config.BaseURL + params);
    
    // const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(JSON.stringify(params))

    console.log(config.BaseURL + percentEncodedJsonData);
    return this.http.get(config.BaseURL + percentEncodedJsonData,null,null)
  }


  sendErrorToServer(errorMessage: string, query: string) {
    let params = {
      service: btoa('app_debug'),
      debug_msg: btoa(errorMessage),
      debug_qry: btoa(query)
    }
    const jsonData = JSON.stringify(params)
    const percentEncodedJsonData = encodeURIComponent(jsonData)
    console.log('encoded ', config.BaseURL + percentEncodedJsonData);

    // return this.http.post(config.BaseURL, percentEncodedJsonData, this.options)
    //   .then(res => {
    //   return  res
    // })
    // .map(Response => Response.json()).subscribe(myResponse => {

    //   return myResponse
    // });

  }
  setUpHeaderAndOptions() {
    this.headers = new Headers();
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', 'beautyapp.pk');
    this.headers.append('Access-Control-Allow-Methods', 'GET,POST')
    this.headers.append('Access-Control-Allow-Headers', 'Authorization, Content-Type')

    // this.options =  new RequestOptions({ headers: this.headers });
  }

  // getCustomerData(): any {
  //   this.storage.get('customer').then((customer) => {
  //     return customer;
  //   }).catch((Error) => {
  //     console.log('Error', 'in login');
  //   });

  // }



  setTabsIndex(value) {
    this.TabsIndex = value;
  }
  getTabsIndex() {
    return this.TabsIndex;
  }

  setTabBar(value) {
    this.isHideTab = value;
  }
  getTabbar() {
    return this.isHideTab;
  }

  getTokenAndCallApi() {
  }



  getCurrentDateTime() {
    let date = new Date();
    let dateNow = this.datePipe.transform(date, 'yyyy-MM-dd HH:mm:ss');
    // console.log("dateHere", dateNow);
    return dateNow;
  }
  getAnnoucementExpiryDate(dateString: string): string {
    
    if (dateString == '0000-00-00 00:00:00') { 
      const format = 'yyyy-MM-d'
      const currentDate = new Date();
      return this.datePipe.transform(currentDate, format)
    }

    var timestamp = Date.parse(dateString);
    console.log(timestamp);

    

    let date = new Date(dateString);

    let expirayDate = this.datePipe.transform(date, 'yyyy-MM-dd');
    return expirayDate;

  }
 

}
