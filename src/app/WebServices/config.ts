
export function getBaseURL(): string {
    let apiMode = Number(localStorage.getItem('zahidzahidzahid'))
    console.log('api mode is : ' + apiMode);
    // apiMode = 2 //2 mean dev
  
    switch (apiMode) {
      case 1:
      return 'http://nopso.qwpcorp.com/noticeup/apis/'
    
      case 2:  
      return 'http://nopso.qwpcorp.com/noticeup/apis/'
  
      case 3:
        return 'http://nopso.qwpcorp.com/noticeup/apis/'
        
      default:
        return 'http://nopso.qwpcorp.com/noticeup/apis/'
    }
  }
  
  export const Base = {
    url: getBaseURL(),
    laravelUrl: 'https://www.laravel.nopso.qwpcorp.com/InstaApi/public/api/pk/'
  };
  export const config = {
    custImg: getBaseURL() + 'customers/',
    TechImageURL: getBaseURL() + 'technicians/',
    salonImgUrl: getBaseURL() + 'salonimages/',
    BaseURL: getBaseURL() + 'json01.php?json=',
    BeautyTipsImageURL: getBaseURL() + 'beautytips_images/',
    CategoryImageURL: getBaseURL() + 'cat_images/',
    prouductImages: getBaseURL() + 'products_images/',
    FashionImageURL: getBaseURL() + 'fashion_images/',
  
    StylesImagesURL: 'https://nopso.qwpcorp.com/salon-laravel-admin/public/category_images/',
    GetSalonFromPinURL: Base.laravelUrl + 'getSalonsFromPinId/',
    getSalonsByPCodeURL: Base.laravelUrl + 'getSalonsByPCode/',
    GetFilteredPinsURL: Base.laravelUrl + 'getFilteredPinsFor/',
    GetAllSubStylesURL: Base.laravelUrl + 'serviceSubCat',
    GetAllServiceCatogries: Base.laravelUrl + 'getAllCategories',
    GetLatLngOfZipCodeURL: Base.laravelUrl + 'getLatLng/',
    salonAssociatedWithPinURL: Base.laravelUrl + 'getPinAssociatedSalons/',
    salonServicesURL: Base.laravelUrl + 'getSalonServices/',
    getPinsForSubStyleURL: Base.laravelUrl + 'getPinsForSubStyle/',
    salonServicesSubCategoriesURL: Base.laravelUrl + 'getSalonServicesSubCategories/',
    makePostFavoriteURL: Base.laravelUrl + 'makePostFavorite/',
    getPostForSubCategory: Base.laravelUrl + 'getPostForSubCategory/',
    getPostForCategory: Base.laravelUrl + 'getPostForCategory/',
    getFashionBrandDetails: Base.laravelUrl + 'getFashionBrandDetailsPag/',
    getFashionCollectionDetails: Base.laravelUrl + 'getFashionCollectionDetailsPag/',
  
    GetPostsBySubCategoryURL: Base.laravelUrl + 'getPostsBySubCat/',
    getFavoritePostsFromServer: Base.laravelUrl + 'getFavoritePosts/',
  
    shareProductURL: 'https://www.beautyapp.pk/products/',
    shareLatestTrendsURL: 'https://www.beautyapp.pk/trends/',
    shareBeautyTipsURL: 'https://www.beautyapp.pk/tips/',
    shareLatestFashionURL: 'https://www.beautyapp.pk/fashion-trends/',
    // GoogleAnalyticsAppID: 'UA-122304371-1',
    GoogleAnalyticsAppID: 'UA-134065188-1',
  
    pakistan: { "cities": ["Ahmadpur East", " Ahmed Nager Chatha", " Ali Khan Abad", " Alipur", " Arifwala", " Attock", " Bhera", " Bhalwal", " Bahawalnagar", " Bahawalpur", " Bhakkar", " Burewala", " Chillianwala", " Choa Saidanshah", " Chakwal", " Chak Jhumra", " Chichawatni", " Chiniot", " Chishtian", " Chunian", " Dajkot", " Daska", " Davispur", " Darya Khan", " Dera Ghazi Khan", " Dhaular", " Dina", " Dinga", " Dhudial Chakwal", " Dipalpur", " Faisalabad", " Fateh Jang", " Ghakhar Mandi", " Gojra", " Gujranwala", " Gujrat", " Gujar Khan", " Harappa", " Hafizabad", " Haroonabad", " Hasilpur", " Haveli Lakha", " Jalalpur Jattan", " Jampur", " Jaranwala", " Jhang", " Jhelum", " Kallar Syedan", " Kalabagh", " Karor Lal Esan", " Kasur", " Kamalia", " Kāmoke", " Khanewal", " Khanpur", " Khanqah Sharif", " Kharian", " Khushab", " Kot Adu", " Jauharabad", " Lahore", " Islamabad", " Lalamusa", " Layyah", " Lawa Chakwal", " Liaquat Pur", " Lodhran", " Malakwal", " Mamoori", " Mailsi", " Mandi Bahauddin", " Mian Channu", " Mianwali", " Miani", " Multan", " Murree", " Muridke", " Mianwali Bangla", " Muzaffargarh", " Narowal", " Nankana Sahib", " Okara", " Renala Khurd", " Pakpattan", " Pattoki", " Pindi Bhattian", " Pind Dadan Khan", " Pir Mahal", " Qaimpur", " Qila Didar Singh", " Rabwah", " Raiwind", " Rajanpur", " Rahim Yar Khan", " Rawalpindi", " Sadiqabad", " Sagri", " Sahiwal", " Sambrial", " Samundri", " Sangla Hill", " Sarai Alamgir", " Sargodha", " Shakargarh", " Sheikhupura", " Shujaabad", " Sialkot", " Sohawa", " Soianwala", " Siranwali", " Tandlianwala", " Talagang", " Taxila", " Toba Tek Singh", " Vehari", " Wah Cantonment", " Wazirabad", " Yazman", " Zafarwal",] },
    blockTimeOnInCorrectCode: 60,
    blockTimeOnResendCode: 60,
    defaultLatitude: '31.5204',
    defaultLongitude: '74.3587',
    FavoritePage: 'Favorite-Salon.ts',
    HomePage: 'Home.ts',
    SalonDetailPage: 'Salon-Detail.ts',
    PinRelatedSalonPage: 'Pin-Action-Sheet.ts',
    ConfirmBookingPage: 'Confirm-Booking.ts',
    BeautyTipsDetailPage: 'BeautyTipsDetailPage',
    BeautyTipsViewPage: 'BeautyTipsViewPage',
    ProductCategoriesPage: 'ProductCategoriesPage',
    ProductsPage: 'ProductsPage',
    distance: 3,
    maxCartPrice: 10000,
    deliveryCharges: 125,
  }
  