import { TestBed } from '@angular/core/testing';

import { ServiceManager } from './service-manager.service';

describe('ServiceManager', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServiceManager = TestBed.get(ServiceManager);
    expect(service).toBeTruthy();
  });
});
