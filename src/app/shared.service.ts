import { Injectable } from '@angular/core';
import { loginResponse, MussaliLoginInterface, PlacesListing } from './Helper classes/Interfaces/interface';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  // public tabIndex: string = "0";
  // public search_keyword: string = "";
  // public sty_id: string = "";
  public GCM_TOKEN: string = "";
  public loggedInAdmin: loginResponse = null
  public MussaliLogin: MussaliLoginInterface = null
  public selectedPlaceDetail: PlacesListing = null
  constructor() { 

  }
}





