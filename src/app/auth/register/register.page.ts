import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceManager } from '../../WebServices/service-manager.service';
import { MOSQUE_REGISTER_INTERFACE, loginResponse } from '../../Helper classes/Interfaces/interface';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SharedService } from '../../shared.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  mosqueName = ''
  mosqueEmail = ''
  mosquePassword = ''
  mosqueConfirmPassword = ''
  mosqueLatitude = ''
  mosqueLongitude = ''
  mosqueAboutUs = ''
  constructor(
    private router: Router,
    private serviceManager: ServiceManager,
    // private geolocation: Geolocation
    public shared: SharedService,
    private navControler: NavController,
  ) { }

  ngOnInit() {

    
    // this.mosqueName = 'zahid Shabbir'
    // this.mosqueEmail = 'salonappnopso@gmail.com'
    // this.mosquePassword = 'zahid123456'
    // this.mosqueConfirmPassword = 'zahid123456'
    // this.mosqueName = "Umar Bin Khattab"
    // this.mosqueAboutUs = "Lorem Ipsum is simply dummy text"
    // this.fillWithDummyData()
    if (this.shared.loggedInAdmin) {
      let place = this.shared.loggedInAdmin
      this.mosqueName = place.p_name
      this.mosqueEmail = place.p_email
      this.mosquePassword =
        this.mosqueConfirmPassword = ''
      this.mosqueLatitude = ''
      this.mosqueLongitude = ''
      this.mosqueAboutUs = place.p_description
    }
    // this.geolocation.getCurrentPosition().then((resp) => {
    //   console.log(resp.coords.latitude);
    //   console.log(resp.coords.longitude);
    //   this.mosqueLatitude = resp.coords.latitude.toString()
    //   this.mosqueLongitude = resp.coords.longitude.toString()

    // }).catch((error) => {
    //   console.log('Error getting location', error);
    // });

    //   let watch = this.geolocation.watchPosition();
    //   watch.subscribe((data) => {
    //     // data can be a set of coordinates, or an error (if an error occurred).
    //     // data.coords.latitude
    //     // data.coords.longitude
    //   });
  }

  ionViewWillLeave() {

    

  }
  registerButtonTapped() {
    if (this.mosqueName.trim().length == 0) {
      this.serviceManager.makeToastOnFailure('Mosque name is required.')
      return
    }
    if (this.mosqueEmail.trim().length == 0) {
      this.serviceManager.makeToastOnFailure('Email is required.')
      return
    }
    if (this.mosquePassword.trim().length == 0) {
      this.serviceManager.makeToastOnFailure('Password is required.')
      return
    }
    // if (this.mosquePassword != this.mosqueConfirmPassword) { 
    //   this.serviceManager.makeToastOnFailure('Passwords do not match.')
    //   return 
    // }
    if (this.mosqueAboutUs.trim().length == 0) {
      this.serviceManager.makeToastOnFailure('Please fill up some information about mosque.')
      return
    }

    var pID = this.shared.loggedInAdmin.p_id
    if (!pID) {
      pID = ""
    }
    let params = {
      service: btoa('register_place'),
      p_name: btoa(this.mosqueName),
      p_email: btoa(this.mosqueEmail),
      p_id: btoa(pID),
      p_password: btoa(this.mosquePassword),
      p_city: btoa('Ilford'),
      cust_device_type: btoa('1'),
      p_lat: btoa(this.mosqueLatitude),
      p_lng: btoa(this.mosqueLongitude),
      p_phone: btoa('020 8553 5739'),
      p_contact: btoa('020 8553 5739'),
      p_description: btoa(this.mosqueAboutUs),
      p_fcm: btoa('23rj32lkjdsf83kljfsoie3nlekjfuow3knfviosuh3jrknwefhy324inrjfsdiyw2lj23jlfwejo32nlcjfhoie')

    }
    this.serviceManager.showProgress("Signing up...")
    this.serviceManager.getData(params).then(res => {
      this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) {
        debugger
        let response: loginResponse = JSON.parse(res.data)
        if (response.status == '1') {
          console.log('Register Done')
          
          
          let _tmep = this.shared.loggedInAdmin
          _tmep.p_name = this.mosqueName
          _tmep.p_email = this.mosqueEmail
          _tmep.p_description = this.mosqueAboutUs
          this.shared.loggedInAdmin = _tmep

          this.serviceManager.setInLocalStorage(this.serviceManager.ADMIN_DATA,this.shared.loggedInAdmin)
          this.clearInputFields()
          if (this.shared.loggedInAdmin && this.shared.loggedInAdmin.p_id.trim().length >= 0) {
            this.serviceManager.makeToastOnSuccess("Your infirmation is updated..")

            this.serviceManager.setInLocalStorage('this.shared.placeDataUpdated', true)
            this.navControler.navigateRoot("admin")
          } else {
            this.serviceManager.setInLocalStorage('this.shared.placeDataUpdated', false)
            this.serviceManager.makeToastOnSuccess("Masjid is registered.")
          }
        }
      }
    }, err => {
      console.log(err);

      this.serviceManager.makeToastOnSuccess("Something went wrong")
    })

      /** 
      .subscribe((res: loginResponse) => {
      console.log(JSON.stringify(res));
      if (res.status == '1') {
        console.log('Register Done')
        
        this.clearInputFields()
        let dd = this.shared.loggedInAdmin
        dd.p_name = this.mosqueName
        dd.p_email = this.mosqueEmail
        dd.p_description = this.mosqueAboutUs
        this.shared.loggedInAdmin = dd
        if (this.shared.loggedInAdmin) {
          this.serviceManager.makeToastOnSuccess("Masjid detail updated.")
          this.shared.loggedInAdmin = res
          this.navCtrl.navigateRoot("admin")

        } else { 
          this.serviceManager.makeToastOnSuccess("Masjid is registered.")
        }
        // this.router.navigateByUrl('auth-login')
        // this.router.navigateByUrl('admin');
      } else {
        this.serviceManager.makeToastOnFailure("something went wrong.")
        // alert('looks like got an error')
      }

    })
    */


    
  }

  loginButtonTapped(email: string, password: string) {
    let params = {
      service: btoa('auth_admin'),
      p_email: btoa(email),
      p_password: btoa(password)
    }

    this.serviceManager.getData(params).then(res => {

      if (res.status = 200 && res.data) {
        let response: loginResponse = JSON.parse(res.data)
        if (response.status == '1') {
          this.serviceManager.makeToastOnSuccess("Information updated.")
          this.shared.loggedInAdmin = response
          this.serviceManager.setInLocalStorage(this.serviceManager.ADMIN_DATA, response)
          this.navControler.pop()

        }
      }
    }, err => {
      console.log(err);

      this.serviceManager.makeToastOnSuccess("Something went wrong")
    })

  }

  clearInputFields() {
    this.mosqueName = ''
    this.mosqueEmail = ''
    this.mosquePassword = ''
    this.mosqueConfirmPassword = ''
    this.mosqueName = ""
    this.mosqueAboutUs = ""
  }

  popPage() { 

    this.router.dispose()
    this.navControler.pop()
    // this.navControler.navigateRoot("admin")
  }
  fillWithDummyData() {
    this.mosqueName = 'Ilford Islamic Centre'
    this.mosqueEmail = ' ilford-islamic-centre@hotmail.co.uk'
    this.mosquePassword = ' ilford-islamic-centre'
    this.mosqueConfirmPassword = 'ilford-islamic-centre'
    this.mosqueAboutUs = `Ilford Islamic Centre is the largest and most famous Mosque in Redbridge and is now very well known not just in London but throughout the country. It has developed over the years into a centre which has become a focal point for the muslims of Redbridge, providing a variety of facilities to serve the community.

    Situated in the heart of South Ilford and within close proximity to Ilford Town Centre, the Mosque is easily accessible by all forms of transport and this in turn results in large congregations especially during Jumah prayer time.
    
    The Masjid has a large number of members who every three years engage in a transparent and thorough election process to elect a Management Committee – this consists of 11 members and they are responsible for making the decisions which effect the day to day running of the Mosque. You can see a list of all the services here.
    
    The role of the mosque extends beyond that of providing for Jumah and the obligatory prayers. The IIC sits on many community and voluntary committees and has representation on the East London Three faiths Forum, Redbridge Racial Equality Council, Redbridge Council of Faiths, Standing Advisory Council on Religious Education (SACRE), Redbridge Police Consultative Committee Group, Racist Incident Panel, Redbridge Voluntary Sector Network, the Muslim Council of Britain (MCB) and Helping Hands.`
  }
}
