import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceManager } from '../../WebServices/service-manager.service'
import { loginResponse } from 'src/app/Helper classes/Interfaces/interface';
import { LoadingController, NavController } from '@ionic/angular';
import { SharedService } from 'src/app/shared.service';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions/ngx';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  //Global variables
  userEmail = ''
  userPassword = ''
  constructor(
    private router: Router,
    private serviceManager: ServiceManager,
    public loadingController: LoadingController,
    public shared: SharedService,
    private navCtrl: NavController,
    private nativePageTransitions: NativePageTransitions
  ) { }


  ngOnInit() {
    // alert('going to call api')
    // this.userEmail = 'salonappnopso@gmail.com'
    // this.userPassword = 'zahid123456'

    // this.userEmail = 'info@barkingmosque.org.uk'
    // this.userPassword = 'info'

  }

  ionViewWillLeave() {

    let options: NativeTransitionOptions = {
       direction: 'up',
       duration: 1500,
       slowdownfactor: 3,
       slidePixels: 20,
       iosdelay: 100,
       androiddelay: 150,
       fixedPixelsTop: 0,
       fixedPixelsBottom: 60
      }
   
    this.nativePageTransitions.slide(options)
      .then()
      .catch();
   
   }

  loginButtonTapped() {
   let params = {
      service: btoa('auth_admin'),
      p_email: btoa(this.userEmail),
      p_password: btoa(this.userPassword)
    }
    this.serviceManager.showProgress("Authenticating...")
    this.serviceManager.getData(params).then(res => {
      this.serviceManager.hideProgress()
      if (res.status = 200 && res.data) {
        let response: loginResponse = JSON.parse(res.data)
        if (response.status == '1') {
          this.serviceManager.setInLocalStorage(this.serviceManager.LOGGED_IN_AS_Admin, true)
          this.shared.loggedInAdmin = response
          this.serviceManager.setInLocalStorage(this.serviceManager.ADMIN_DATA,response)
          console.log('login done')

          this.navCtrl.navigateRoot("admin")
        } else {

          this.serviceManager.makeToastOnFailure(res.error)
        }
      }
    }, err => {
      this.serviceManager.hideProgress()
      console.log(err);

      this.serviceManager.makeToastOnSuccess("Something went wrong")
    })

  }
  login(form) {
    console.log(this.userEmail);

    // this.authService.login(form.value).subscribe((res)=>{
    this.router.navigateByUrl('admin');
    // });
  }
}
