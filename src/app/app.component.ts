import { Component } from '@angular/core';

import { Platform, AlertController, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { FCM } from '@ionic-native/fcm/ngx';
import { ServiceManager } from './WebServices/service-manager.service';
import { Events } from '@ionic/angular';
import { SharedService } from './shared.service';

import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { MussaliLoginInterface, loginResponse } from './Helper classes/Interfaces/interface';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public gcmToken = ''
  gpsOptions = { maximumAge: 300000, timeout: 10000, enableHighAccuracy: false };
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private fcm: FCM,
    public serviceManager: ServiceManager,
    public events: Events,
    public shared: SharedService,

    public diagnostic: Diagnostic,
    private androidPermissions: AndroidPermissions,
    // public geolocation: Geolocation,
    public alertCtrl: AlertController,
    private navCtrl: NavController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.initFcm()
      this.gcmToken = this.serviceManager.getFromLocalStorage(this.serviceManager.GCM_TOKEN)
      this.routeUserToRespectively()
    });
  }

  initFcm() {
    this.fcm.subscribeToTopic('marketing');
    this.fcm.onNotification().subscribe(data => {
      this.events.publish('NeedToUpdate', true);
      if (data.wasTapped) {
        // this.goToNotificationsTab()
        console.log("Received in background");
      } else {
        console.log("Received in foreground");
      };
    })
    if (!this.gcmToken || this.gcmToken === null) {
      this.fcm.getToken().then((token) => {
        if (token && token.toString().length > 5) {
          this.gcmToken = token
          // this.serviceManager.makeToastOnSuccess("got token 57 from local storage")
          this.shared.GCM_TOKEN = token
          console.log(token);
          // alert('token in 248 on gettoken() ' + token);
          this.serviceManager.setInLocalStorage(this.serviceManager.GCM_TOKEN, token);
        } else {
          // alert('token seems null')
          console.log('token seems null')
        }
      }).catch(error => {
        console.log('ErrorInToken' + JSON.stringify(error));
      });
      // added on on 13-09-18 end get token function on 13-09-18
      this.fcm.onTokenRefresh().subscribe(token => {
        if (token && token.toString().length > 5) {
          console.log('going tosave token on onTokenRefresh()');
          // this.serviceManager.makeToastOnSuccess("got token 73 on token refresh")
          // alert('going tosave token on onTokenRefresh()');
          console.log(token);
          // alert('token in 266 on refreshtoken() ' + token);
          if (!this.gcmToken || this.gcmToken.toString().length < 5) {
            this.serviceManager.setInLocalStorage(this.serviceManager.GCM_TOKEN, token);
            console.log('fetching saved token');
            this.shared.GCM_TOKEN = token
          }
        } else {
          console.log('token seems null');
        }
      })
    }
  }
  routeUserToRespectively() {

    let isMussali: boolean = this.serviceManager.getFromLocalStorage(this.serviceManager.LOGGED_IN_AS_MUSSALI)
    // alert('isMussali ' + isMussali)
    if (isMussali && isMussali == true) {
      // debugger
      // let aa = this.serviceManager.getFromLocalStorage(this.serviceManager.CUSTOMER_DATA)
      // console.log(aa);
      
      this.shared.MussaliLogin = this.serviceManager.getFromLocalStorage(this.serviceManager.CUSTOMER_DATA)
      // alert(this.shared.MussaliLogin.u_id)
      this.navCtrl.navigateRoot('mussali-landing-page');
    } else {
      let isAdmin: boolean = this.serviceManager.getFromLocalStorage(this.serviceManager.LOGGED_IN_AS_Admin)
      // alert('isAdmin ' + isAdmin)
      if (isAdmin && isAdmin == true) {
        this.shared.loggedInAdmin = this.serviceManager.getFromLocalStorage(this.serviceManager.ADMIN_DATA)
        this.navCtrl.navigateRoot('admin')
      } else {
        // alert("base route")
        this.navCtrl.navigateRoot('')
      }
    }




  }
  gotLocation(location) {
    // alert('got location')
  }
  gotProb(location) {
    // alert("got probs")
  }
  // fetchLocation() { 
  //   // alert("came location 119")
  //   if (this.platform.is('android')) {
  //     // alert('platform is android')
  //     this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.ACCESS_FINE_LOCATION])

  //     this.geolocation.getCurrentPosition().then(location => {
  //       // alert('got locatin 123 xompo')
  //     }, onError => {

  //         this.diagnostic.isGpsLocationAvailable().then(isLocationEnabled => {
  //         // alert("is location available "+isLocationEnabled)
  //         isLocationEnabled ? this.getCurrenctLocation() : this.confirmSwitchToSetting()

  //       })
  //       this.platform.resume.subscribe(() => {

  //         this.platform.ready().then(() => {
  //           this.diagnostic.isGpsLocationAvailable().then(isLocationEnabled => {

  //             isLocationEnabled ? this.getCurrenctLocation() : ''
  //             // this.serviceManager.makeToastOnFailure('BeautyApp is unable to get location permission. Please allow location permission manually. Please review the message.')
  //           })
  //         })

  //       });
  //     })

  //   } else {
  //     // alert('platform is not android')
  //     this.getCurrenctLocation()
  //   }
  // }

  // getCurrenctLocation() {
  //   // alert('platform is not android')
  //   this.geolocation.getCurrentPosition(this.gpsOptions).then(location => {


  //   }, onError => {

  //   })
  // }
  confirmSwitchToSetting() {
    // alert("you hae to switch to setting")
    return
    // if (this.platform.is('android')) {
    //   let alert = this.alertCtrl.create({
    //     title: 'Device location request',
    //     message: 'Your device location service is not enabled for BeautyApp.  Please allow BeautyApp to access your location.',
    //     buttons: [
    //       {
    //         text: 'Deny',
    //         role: 'cancel',
    //         handler: () => {
    //           console.log('Cancel clicked');

    //         }
    //       },
    //       {
    //         text: 'Allow',
    //         handler: () => {
    //           console.log('Yes clicked');

    //           this.diagnostic.switchToLocationSettings()
    //         }
    //       }
    //     ]
    //   });
    //   alert.present().then(() => {

    //   });
    // }

  }
}
