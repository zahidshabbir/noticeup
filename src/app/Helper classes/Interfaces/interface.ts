export interface MasjidObject{
    id?: number,
    name?: string,
    image?: string,
  description?: string,
  liked?: boolean,
}
  
export interface NoticeUp {
  id?: number, 
  title?: string
  message?: string,
  noticeDate?: string
}

export interface loginResponse {
  json_status?: loginJSONStatus;
  status?: string;
  p_id: string;
  p_name: string;
  p_email: string;
  p_phone?: string;
  p_address?: string;
  p_city?: string;
  p_description: string;
  p_status?: string;
  response_datetime?: string;


  
}
export interface loginJSONStatus {
  type: string;
  value: string;
}




  export interface MOSQUE_REGISTER_STATUS {
      type: string;
      value: string;
  }

  export interface MOSQUE_REGISTER_INTERFACE {
      json_status: MOSQUE_REGISTER_STATUS;
      p_image_name: string;
      status: string;
      message: string;
      response_datetime: string;
  }

// login musslai internface
  export interface MussaliStatus {
      type: string;
      value: string;
  }

  export interface MussaliLoginInterface {
      json_status: MussaliStatus;
      u_id: number;
      status: string;
      message: string;
    response_datetime: string;
    error?: string
  }



/// Favorite annoucment


  export interface FavoriteJsonStatus {
      type: string;
      value: string;
  }

  export interface Announcement {
      a_id: string;
      a_title: string;
      a_description: string;
      a_datetime: string;
      a_expiry: string;
      p_id: string;
  }

  export interface FavoriteAnnoucementInterface {
      json_status: FavoriteJsonStatus;
      announcements: Announcement[];
      response_datetime: string;
  }




// new annoucemnt interface
export interface NewAnnoucenmentType {
  type: string;
  value: string;
}

export interface NewAnnoucenment {
  json_status: NewAnnoucenmentType;
  status: string;
  message: string;
  response_datetime: string;
}

/// annoucemnt listing

export interface JsonStatus {
  type: string;
  value: string;
}

export interface AnnouncementListing {
  a_id: string;
  a_title: string;
  a_description: string;
  a_datetime: string;
  a_expiry: string;
  p_id: string;
}

export interface AnnouncementListinResponse {
  json_status: JsonStatus;
  announcements: AnnouncementListing[];
  response_datetime: string;
}


/// MOSQUE LISTING INTERFACE

  export interface MosquesomeSTatus {
      type: string;
      value: string;
  }

  export interface PlacesListing {
      p_id?: any;
      p_name: string;
      p_email?: string;
      p_password?: string;
      p_image?: string;
      p_lat?: string;
      p_lng?: string;
      p_phone?: string;
      p_contact?: string;
      p_address?: string;
      p_city?: string;
      p_description?: string;
      p_fcm?: string;
      p_status?: string;
      favplaces?: string;
      distance?: string;
      
  }

  export interface MosqueResponseInterface {
      json_status: MosquesomeSTatus;
      server_datetime: string;
      status: string;
      places: PlacesListing[];
      response_datetime: string;
  }








    export interface UPDATE_PLACE_MODEL_RESPONSE {
        json_status: JsonStatus;
        p_image_name: string;
        place: Place;
        status: string;
        message: string;
        response_datetime: string;
    }


    export interface Place {
      p_id?: string;
      p_name?: string;
      p_email?: string;
      p_password?: string;
      p_image?: string;
      p_lat?: string;
      p_lng?: string;
      p_phone?: string;
      p_contact?: string;
      p_address?: string;
      p_city?: string;
      p_description?: string;
      p_fcm?: string;
      p_status?: string;
  }