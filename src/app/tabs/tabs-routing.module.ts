import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'announcement',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../pages/announcement/announcement.module').then( m => m.AnnouncementPageModule)
          }
        ]
      },
      {
        path: 'mosque',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../pages/mosque/mosque.module').then( m => m.MosquePageModule)
          }
        ]
      },
      {
        path: 'about-us',
        children: [
          {
            path: '',
            loadChildren: () =>
            import('../pages/about-us/about-us.module').then( m => m.AboutUsPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/announcement',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/announcement',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
